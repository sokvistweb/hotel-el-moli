jQuery(document).ready(function($){
	
    
    // Open/close submenu on mobile
	$('.main-nav, #nav a').on('click', function(event){
		if($(event.target).is('.main-nav, #nav a')) $(this).children('ul').toggleClass('is-visible');
	});
    
    
    // One Page Nav
    $('#nav').onePageNav({
		currentClass: 'current-menu-item',
		changeHash: false,
		scrollSpeed: 1500,
		scrollThreshold: 0.5,
		filter: '',
		easing: 'swing'
	});
    
    
    // Smooth scrolling
    $('a.discover').click(function() {
        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top
        }, {
            duration: 1500,
            easing: 'swing'
        });
        return false;
    });
    
    
    // Animated header
    var changeHeader = $(window).height() * 0.25;
    $(window).scroll(function() {
        var scroll = getCurrentScroll();
        if ( scroll >= changeHeader ) {
            $('.skv-header').addClass('scrolled');
            }
            else {
                $('.skv-header').removeClass('scrolled');
            }
    });
    
    function getCurrentScroll() {
        return window.pageYOffset;
    }
    
    
    
    // Modal popup
    // A simple jQuery modal (http://github.com/kylefox/jquery-modal)
    $('a.offers').click(function(event) {
        $(this).modal({
            fadeDuration: 250,
            fadeDelay: 1
        });
        return false;
    });
    
    
    
    // http://responsiveslides.com v1.54 by @viljamis
    // Slideshow 1
    $("#slider1").responsiveSlides({
        manualControls: '#slider1-pager',
        auto: false,
        maxwidth: 768
    });

    // Slideshow 2
    $("#slider2").responsiveSlides({
        manualControls: '#slider2-pager',
        auto: false,
        maxwidth: 768
    });
    
    
    // Swipebox Gallery
    // http://brutaldesign.github.io/swipebox/
    $('.swipebox').swipebox({
		useCSS : true, // false will force the use of jQuery for animations
		hideBarsDelay : 1600 // 0 to always show caption and action bar
	});
    
    
    // Back to top
    // browser window scroll (in pixels) after which the "back to top" link is shown
	var offset = $(window).height() * 1.5,
		//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
		offset_opacity = $(window).height() * 2,
		//duration of the top scrolling animation (in ms)
		scroll_top_duration = 3000,
		//grab the "back to top" link
		$back_to_top = $('#back-to-top');

	//hide or show the "back to top" link
	$(window).scroll(function(){
		( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
		if( $(this).scrollTop() > offset_opacity ) { 
			$back_to_top.addClass('cd-fade-out');
		}
	});
    
    //smooth scroll to top
    $back_to_top.on('click', function(event){
        event.preventDefault();
        $('body,html').animate({
            scrollTop: 0,
            }, scroll_top_duration
        );
    });
    
    
});
