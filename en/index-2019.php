<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="en"><!--<![endif]-->
<head>
    <meta charset="utf-8">
	<title>Hotel El Molí</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="Family hotel in the Costa Brava, located 2 minutes from the beach of Sant Pere Pescador and the natural park of Aiguamolls del Empordà." />
    <meta name="keywords" content="Hotel, Costa Brava, Girona, Empordà, Sant Pere Pescador" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="author" href="/humans.txt">
    <link rel="shortcut icon" href="/favicon.ico" />
    <link rel="apple-touch-icon" href="/apple-touch-icon.png" /><!-- 57×57px -->
    <link rel="apple-touch-icon-precomposed" href="/apple-touch-icon-precomposed.png"><!-- 180×180px -->
    
    <link rel="stylesheet" href="/assets/css/style.css">
	<link href='http://fonts.googleapis.com/css?family=Roboto:500,700,300' rel='stylesheet' type='text/css'>
    
    <link rel="canonical" href="http://hotelelmoli.com/en">
    <link hreflang="ca" href="http://hotelelmoli.com/" rel="alternate">
    <link hreflang="es" href="http://hotelelmoli.com/es/" rel="alternate">
    <link hreflang="fr" href="http://hotelelmoli.com/fr/" rel="alternate">
    <link hreflang="de" href="http://hotelelmoli.com/de/" rel="alternate">
    
    <!-- Open Graph -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="Hotel El Molí">
    <meta property="og:description" content="Family hotel in the Costa Brava, located 2 minutes from the beach of Sant Pere Pescador and the natural park of Aiguamolls del Empordà.">
    <meta property="og:image" content="http://hotelelmoli.com/assets/images/web-og-image.jpg">
    <meta property="og:url" content="http://hotelelmoli.com/">
    <meta property="og:site_name" content="Hotel El Molí">
    <meta property="og:locale" content="ca_ES">
    
    <!-- Google+ / Schema.org 
    <link href="https://plus.google.com/+YourPage" rel="publisher">
    <meta itemprop="name" content="Hotel El Molí">
    <meta itemprop="description" content="Family hotel in the Costa Brava, located 2 minutes from the beach of Sant Pere Pescador and the natural park of Aiguamolls del Empordà.">
    <meta itemprop="image" content="http://hotelelmoli.com/assets/images/og-image.jpg">
    -->

    <!-- Modernizr -->
	<script async src="/assets/js/vendor/modernizr-2.7.1.min.js"></script>
  	
	
</head>
<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status" class="spinner">
            <div id="loading">
                <div id="loading-center">
                    <div id="loading-center-absolute">
                        <div class="object" id="object_one"></div>
                        <div class="object_black" id="black_one"></div>
                        <div class="object" id="object_two"></div>
                        <div class="object_black" id="black_two"></div>
                        <div class="object" id="object_three"></div>
                        <div class="object_black" id="black_three"></div>
                        <div class="object" id="object_four"></div>
                        <div class="object_black" id="black_four"></div>
                        <div class="object" id="object_five"></div>
                        <div class="object_black" id="black_five"></div>
                        <div class="object" id="object_six"></div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- /#preloader -->
    
	<header class="skv-header">
		<div id="logo"><a href="/"><img src="/assets/images/svg/logo-hotel-el-moli.svg" alt="Logo Hotel El Molí"></a></div>
		<nav class="main-nav">
			<ul id="nav">
				<li><a href="#hotel">The Hotel</a></li>
				<li><a href="#habitacions">Rooms</a></li>
				<li><a href="#serveis">Services</a></li>
				<li><a href="#preus">Prices</a></li>
			</ul>
		</nav> <!-- /.main-nav -->
        <nav class="languages">
            <ul>
                <li><a href="/">ca</a></li>
                <li><a href="/es/">es</a></li>
                <li><a href="/fr/">fr</a></li>
                <li><a href="/de/">de</a></li>
            </ul>
        </nav>
        <div class="contact">
            <a href="tel:0034972520069">+34 972 52 00 69</a>
        </div>
	</header>

	<main class="main-content">
        <!-- Hotel starts -->
		<div class="fixed-bg bg-hotel" id="hotel">
			<h1>hotel el molí</h1>
            <div class="more-btn">
                <a href="#wellcome" class="discover"><span>Discover</span></a>
            </div>
		</div> <!-- /.fixed-bg -->

		<div class="scrolling-bg" id="wellcome">
            <div class="container">
                <p class="copy-home">The hotel El Molí is a <strong>family run hotel</strong>, situated in an area of land of 20.000 square meters. It is situated on the outskirts of the village only a few meters from the Fluvià river and the <strong>Aiguamolls de l’Empordà</strong> wetlands. It is close to a very long beach with sun and dunes where you can do windsurfing, <strong>kitesurfing</strong>, scuba diving, hiking as well as being a central point from which to go on sightseeing tours.</p>

                <div class="branches branch1"></div>

                <div class="buttons">
                    <a class="booking" href="https://www.thebookingbutton.co.uk/properties/hotelelmolidirect" target="_blank" title="Bookings page">Booking</a>
                    <a class="offers" href="oferta.php" title="Ofertes" rel="modal:open">Spring Promotion</a>
                </div>
            </div><!-- /.container -->
		</div> <!-- /.scrolling-bg -->
        <!-- Hotel ends -->
        
        
        <!-- Habitacions starts -->
        <div class="fixed-bg fixed-bg-90 bg-rooms" id="habitacions">
            <h2>rooms</h2>
		</div> <!-- cd-fixed-bg -->

		<div class="scrolling-bg">
			<div class="container">
                <div class="trans-content">
                    <p>The hotel has twenty six comfortable rooms, some of which have hydromassage baths and balcony. Some of our other rooms face the garden and the swimming pool, and all of them have satellite TV and air conditioning. Family rooms are also available. Each room has its own special decor.</p>
                    
                    <nav class="rooms-nav">
                        <ul id="roomsnav">
                            <li><a class="booknow" href="#basica">&nbsp;&nbsp;&nbsp; Basic double &nbsp;&nbsp;&nbsp;<br> room</a></li>
                            <li><a class="booknow" href="#estandard">Standard double room</a></li>
                            <li><a class="booknow" href="#hidromassatge">Superior double room</a></li>
                        </ul>
                    </nav>
                    
                    <div class="branches branch2"></div>
                </div>
            </div> <!-- cd-container -->

			<div class="container">
                <div class="slider" id="basica">
                    <h2>Basic double room</h2>
                    <ul class="rslides" id="slider1">
                        <li><img src="/assets/images/rooms/habitacio-basica-1.jpg" alt="Habitació doble bàsica" width="768" height="513"></li>
                        <li><img src="/assets/images/rooms/habitacio-basica-2.jpg" alt="Habitació doble bàsica" width="768" height="513"></li>
                        <li><img src="/assets/images/rooms/habitacio-basica-3.jpg" alt="Habitació doble bàsica" width="768" height="513"></li>
                        <li><img src="/assets/images/rooms/habitacio-basica-4.jpg" alt="Habitació doble bàsica" width="768" height="513"></li>
                    </ul>
                    <!-- Slideshow 3 Pager -->
                    <ul class="slider-pager" id="slider1-pager">
                        <li><a href="#"><img src="/assets/images/rooms/th-habitacio-basica-1.jpg" alt="Habitació doble bàsica" width="120" height="80"></a></li>
                        <li><a href="#"><img src="/assets/images/rooms/th-habitacio-basica-2.jpg" alt="Habitació doble bàsica" width="120" height="80"></a></li>
                        <li><a href="#"><img src="/assets/images/rooms/th-habitacio-basica-3.jpg" alt="Habitació doble bàsica" width="120" height="80"></a></li>
                        <li><a href="#"><img src="/assets/images/rooms/th-habitacio-basica-4.jpg" alt="Habitació doble bàsica" width="120" height="80"></a></li>
                    </ul>
                    
                    <p>One double bed (135cm) private bathroom, flat-screen TV, hairdryer, free toiletries, Satellite Channels, free Wi-Fi, air conditioning, 10m2 approx.</p>

                    <a class="booknow" href="https://www.thebookingbutton.co.uk/properties/hotelelmolidirect" target="_blank" title="Bookings page">Book now</a>
                </div> <!-- .slider -->
                
                <div class="slider" id="estandard">
                    <h2>Standard double room</h2>
                    <ul class="rslides" id="slider2">
                        <li><img src="/assets/images/rooms/habitacio-estandard-1.jpg" alt="Habitació doble estandard" width="768" height="513"></li>
                        <li><img src="/assets/images/rooms/habitacio-estandard-2.jpg" alt="Habitació doble estandard" width="768" height="513"></li>
                        <li><img src="/assets/images/rooms/habitacio-estandard-3.jpg" alt="Habitació doble estandard" width="768" height="513"></li>
                        <li><img src="/assets/images/rooms/habitacio-estandard-4.jpg" alt="Habitació doble estandard" width="768" height="513"></li>
                        <li><img src="/assets/images/rooms/habitacio-estandard-5.jpg" alt="Habitació doble estandard" width="768" height="513"></li>
                        <li><img src="/assets/images/rooms/habitacio-estandard-6.jpg" alt="Habitació doble estandard" width="768" height="513"></li>
                    </ul>
                    <!-- Slideshow 3 Pager -->
                    <ul class="slider-pager" id="slider2-pager">
                        <li><a href="#"><img src="/assets/images/rooms/th-habitacio-estandard-1.jpg" alt="Habitació doble estandard" width="120" height="80"></a></li>
                        <li><a href="#"><img src="/assets/images/rooms/th-habitacio-estandard-2.jpg" alt="Habitació doble estandard" width="120" height="80"></a></li>
                        <li><a href="#"><img src="/assets/images/rooms/th-habitacio-estandard-3.jpg" alt="Habitació doble estandard" width="120" height="80"></a></li>
                        <li><a href="#"><img src="/assets/images/rooms/th-habitacio-estandard-4.jpg" alt="Habitació doble estandard" width="120" height="80"></a></li>
                        <li><a href="#"><img src="/assets/images/rooms/th-habitacio-estandard-5.jpg" alt="Habitació doble estandard" width="120" height="80"></a></li>
                        <li><a href="#"><img src="/assets/images/rooms/th-habitacio-estandard-6.jpg" alt="Habitació doble estandard" width="120" height="80"></a></li>
                    </ul>
                    
                    <p>One double bed (150cm) or twins, private bathroom , flat-screen TV, hairdryer, free toiletries, Satellite Channels, free Wi-Fi, air conditioning, 14 m2 approx.</p>
                    
                    <a class="booknow" href="https://www.thebookingbutton.co.uk/properties/hotelelmolidirect" target="_blank" title="Bookings page">Book now</a>
                </div> <!-- /.slider -->
                
                <div class="slider" id="hidromassatge">
                    <h2>Superior double room</h2>
                    <ul class="rslides" id="slider3">
                        <li><img src="/assets/images/rooms/habitacio-superior-1.jpg" alt="Habitació doble superior" width="768" height="513"></li>
                        <li><img src="/assets/images/rooms/habitacio-superior-2.jpg" alt="Habitació doble superior" width="768" height="513"></li>
                        <li><img src="/assets/images/rooms/habitacio-superior-3.jpg" alt="Habitació doble superior" width="768" height="513"></li>
                        <li><img src="/assets/images/rooms/habitacio-superior-4.jpg" alt="Habitació doble superior" width="768" height="513"></li>
                        <li><img src="/assets/images/rooms/habitacio-superior-5.jpg" alt="Habitació doble superior" width="768" height="513"></li>
                        <li><img src="/assets/images/rooms/habitacio-superior-6.jpg" alt="Habitació doble superior" width="768" height="513"></li>
                    </ul>
                    <!-- Slideshow 3 Pager -->
                    <ul class="slider-pager" id="slider3-pager">
                        <li><a href="#"><img src="/assets/images/rooms/th-habitacio-superior-1.jpg" alt="Habitació doble superior" width="120" height="80"></a></li>
                        <li><a href="#"><img src="/assets/images/rooms/th-habitacio-superior-2.jpg" alt="Habitació doble superior" width="120" height="80"></a></li>
                        <li><a href="#"><img src="/assets/images/rooms/th-habitacio-superior-3.jpg" alt="Habitació doble superior" width="120" height="80"></a></li>
                        <li><a href="#"><img src="/assets/images/rooms/th-habitacio-superior-4.jpg" alt="Habitació doble superior" width="120" height="80"></a></li>
                        <li><a href="#"><img src="/assets/images/rooms/th-habitacio-superior-5.jpg" alt="Habitació doble superior" width="120" height="80"></a></li>
                        <li><a href="#"><img src="/assets/images/rooms/th-habitacio-superior-6.jpg" alt="Habitació doble superior" width="120" height="80"></a></li>
                    </ul>
                    
                    <p>One double bed (150cm) private bathroom , spa bath, air Conditioning, flat-screen TV, hairdryer, free toiletries, Satellite Channels, free Wi-Fi, 20 m2 approx.</p>
                    
                    <a class="booknow" href="https://www.thebookingbutton.co.uk/properties/hotelelmolidirect" target="_blank" title="Bookings page">Book now</a>
                </div> <!-- /.slider -->
            </div> <!-- /.container -->
		</div> <!-- /.scrolling-bg -->
        <!-- Habitacions ends -->
        
        
        <!-- Serveis starts -->
        <div class="fixed-bg fixed-bg-90 bg-services" id="serveis">
			<h2>services</h2>
		</div> <!-- /.fixed-bg -->

		<div class="scrolling-bg">
            <div class="container">
                <p>The hotel has its own parking, swimming pool, children’s playground, a large solarium and a terrace. There is also a Snack Bar open all day, a WI-FI area and a library. The hotel has tennis courts and a garage to keep nautical gear, bicycles …</p>
                <p>El Molí is surrounded by a garden and a pine tree forest, the ideal place to go for a walk or to sit quietly reading.</p>

                <div class="branches branch3 branch2em"></div>
            </div><!-- /.container -->
            
            <div class="container container-1024">
                <div class="gallery">
                    <a href="/assets/images/gallery/hotel-el-moli-02.jpg" class="swipebox thumbnail"><img class="th-image" src="/assets/images/gallery/th_hotel-el-moli-02.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="/assets/images/gallery/hotel-el-moli-03.jpg" class="swipebox thumbnail"><img class="th-image" src="/assets/images/gallery/th_hotel-el-moli-03.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="/assets/images/gallery/hotel-el-moli-04.jpg" class="swipebox thumbnail"><img class="th-image" src="/assets/images/gallery/th_hotel-el-moli-04.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="/assets/images/gallery/hotel-el-moli-05.jpg" class="swipebox thumbnail"><img class="th-image" src="/assets/images/gallery/th_hotel-el-moli-05.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="/assets/images/gallery/hotel-el-moli-09.jpg" class="swipebox thumbnail"><img class="th-image" src="/assets/images/gallery/th_hotel-el-moli-09.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="/assets/images/gallery/hotel-el-moli-10.jpg" class="swipebox thumbnail"><img class="th-image" src="/assets/images/gallery/th_hotel-el-moli-10.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="/assets/images/gallery/hotel-el-moli-11.jpg" class="swipebox thumbnail"><img class="th-image" src="/assets/images/gallery/th_hotel-el-moli-11.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="/assets/images/gallery/hotel-el-moli-12.jpg" class="swipebox thumbnail"><img class="th-image" src="/assets/images/gallery/th_hotel-el-moli-12.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="/assets/images/gallery/hotel-el-moli-13.jpg" class="swipebox thumbnail"><img class="th-image" src="/assets/images/gallery/th_hotel-el-moli-13.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="/assets/images/gallery/hotel-el-moli-14.jpg" class="swipebox thumbnail"><img class="th-image" src="/assets/images/gallery/th_hotel-el-moli-14.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="/assets/images/gallery/hotel-el-moli-15.jpg" class="swipebox thumbnail"><img class="th-image" src="/assets/images/gallery/th_hotel-el-moli-15.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="/assets/images/gallery/hotel-el-moli-16.jpg" class="swipebox thumbnail"><img class="th-image" src="/assets/images/gallery/th_hotel-el-moli-16.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="/assets/images/gallery/hotel-el-moli-17.jpg" class="swipebox thumbnail"><img class="th-image" src="/assets/images/gallery/th_hotel-el-moli-17.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="/assets/images/gallery/hotel-el-moli-18.jpg" class="swipebox thumbnail"><img class="th-image" src="/assets/images/gallery/th_hotel-el-moli-18.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="/assets/images/gallery/hotel-el-moli-19.jpg" class="swipebox thumbnail"><img class="th-image" src="/assets/images/gallery/th_hotel-el-moli-19.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="/assets/images/gallery/hotel-el-moli-20.jpg" class="swipebox thumbnail"><img class="th-image" src="/assets/images/gallery/th_hotel-el-moli-20.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="/assets/images/gallery/hotel-el-moli-21.jpg" class="swipebox thumbnail"><img class="th-image" src="/assets/images/gallery/th_hotel-el-moli-21.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="/assets/images/gallery/hotel-el-moli-22.jpg" class="swipebox thumbnail"><img class="th-image" src="/assets/images/gallery/th_hotel-el-moli-22.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="/assets/images/gallery/hotel-el-moli-23.jpg" class="swipebox thumbnail"><img class="th-image" src="/assets/images/gallery/th_hotel-el-moli-23.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="/assets/images/gallery/hotel-el-moli-24.jpg" class="swipebox thumbnail"><img class="th-image" src="/assets/images/gallery/th_hotel-el-moli-24.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                </div>
            </div><!-- /.container -->
		</div> <!-- /.scrolling-bg -->
        <!-- Servies ends -->

        
        
        <!-- Preus starts -->
        <div class="fixed-bg fixed-bg-90 bg-prices" id="preus">
			<h2>prices</h2>
		</div> <!-- /.fixed-bg -->

		<div class="scrolling-bg">
            <div class="container container-1024">
                
                <!-- Prices -->
                <?php include '../preus.php';?>
                
                <div class="table-columns">
                    <ul class="price">
                        <li class="header">
                            <h2>Low Season</h2>
                            <p>From June 1 to June 30 and <br>from August 26 to September 30</p>
                        </li>
                        <li>
                            <p>Basic double room</p>
                            <span><?=$tb2b?> €</span>
                        </li>
                        <li class="grey">
                            <p>Standard double room</p>
                            <span><?=$tb2e?> €</span>
                        </li>
                        <li>
                            <p>Superior double room</p>
                            <span><?=$tb2s?> €</span>
                        </li>
                        <li class="grey">
                            <p>Triple room</p>
                            <span><?=$tb3e?> €</span>
                        </li>
                        <li>
                            <p>Superior triple room</p>
                            <span><?=$tb3s?> €</span>
                        </li>
                        <li class="grey">
                            <p>Breakfast</p>
                            <span><?=$tbes?> €</span>
                        </li>
                        <li class="grey">
                            <p>Extra bed</p>
                            <span><?=$tbls?> €</span>
                        </li>
                        <li><a href="https://www.thebookingbutton.co.uk/properties/hotelelmolidirect" target="_blank" title="Bookings page" class="button">Book now</a></li>
                    </ul>
                </div>
                
                <div class="table-columns">
                    <ul class="price">
                        <li class="header">
                            <h2>Middle Season</h2>
                            <p>From April 18 to April 22 and <br>from July 1 to July 31</p>
                        </li>
                        <li>
                            <p>Basic double room</p>
                            <span><?=$tm2b?> €</span>
                        </li>
                        <li class="grey">
                            <p>Standard double room</p>
                            <span><?=$tm2e?> €</span>
                        </li>
                        <li>
                            <p>Superior double room</p>
                            <span><?=$tm2s?> €</span>
                        </li>
                        <li class="grey">
                            <p>Triple room</p>
                            <span><?=$tm3e?> €</span>
                        </li>
                        <li>
                            <p>Superior triple room</p>
                            <span><?=$tm3s?> €</span>
                        </li>
                        <li class="grey">
                            <p>Breakfast</p>
                            <span><?=$tmes?> €</span>
                        </li>
                        <li class="grey">
                            <p>Extra bed</p>
                            <span><?=$tmls?> €</span>
                        </li>
                        <li><a href="https://www.thebookingbutton.co.uk/properties/hotelelmolidirect" target="_blank" title="Bookings page" class="button">Book now</a></li>
                    </ul>
                </div>

                <div class="table-columns">
                    <ul class="price">
                        <li class="header">
                            <h2>High Season</h2>
                            <p>From August 1 <br>to August 25</p>
                        </li>
                        <li>
                            <p>Basic double room</p>
                            <span><?=$ta2b?> €</span>
                        </li>
                        <li class="grey">
                            <p>Standard double room</p>
                            <span><?=$ta2e?> €</span>
                        </li>
                        <li>
                            <p>Superior double room</p>
                            <span><?=$ta2s?> €</span>
                        </li>
                        <li class="grey">
                            <p>Triple room</p>
                            <span><?=$ta3e?> €</span>
                        </li>
                        <li>
                            <p>Superior triple room</p>
                            <span><?=$ta3s?> €</span>
                        </li>
                        <li class="grey">
                            <p>Breakfast</p>
                            <span><?=$taes?> €</span>
                        </li>
                        <li class="grey">
                            <p>Extra bed</p>
                            <span><?=$tals?> €</span>
                        </li>
                        <li><a href="https://www.thebookingbutton.co.uk/properties/hotelelmolidirect" target="_blank" title="Bookings page" class="button">Book now</a></li>
                    </ul>
                </div>
                

                <div class="align-right">
                    <p>10% VAT included / + Touristic tax 0.50 € prr person</p>
                </div>
        
                <div class="buttons">
                    <a class="offers" href="oferta.php" title="Ofertes" rel="modal:open">Spring Promotion</a>
                </div>
        
                <div class="branches branch2 branch3em"></div>
        
                <div class="align-center">
                    <p><strong>Reservation: tel. <a href="tel:0034972520069">+34 972 52 00 69</a> · e-mail <a href="tel:0034972520069">+34 972 52 00 69</a> · e-mail <a href="mailto:&#105;&#110;&#102;&#111;&#064;&#104;&#111;&#116;&#101;&#108;&#101;&#108;&#109;&#111;&#108;&#105;&#046;&#099;&#111;&#109;"><!-- encoded - wbwip.com/wbw/emailencoder.html --><!-- info@hotelelmoli.com -->&#105;&#110;&#102;&#111;&#064;&#104;&#111;&#116;&#101;&#108;&#101;&#108;&#109;&#111;&#108;&#105;&#046;&#099;&#111;&#109;</a></strong></p>
                </div>
            </div><!-- /.container -->
		</div> <!-- /.scrolling-bg -->
        <!-- Preus ends -->
        

		<div class="fixed-bg fixed-bg-75 bg-hammock">
		</div> <!-- /.fixed-bg -->
    

		<footer>
            <div class="cont-footer">
                <p>Hotel El Molí | Ctra. de la Platja 3 | 17470 Sant Pere Pescador | Girona, Alt Empordà, Costa Brava <a href="https://goo.gl/maps/fKerikuJARQ2" title="Veure a Google Maps" target="_blank"><img src="/assets/images/svg/ios-location.svg" alt="On som" width="512" height="512"></a></p>
                <p>Tel./Fax: <a href="tel:0034972520069">+34 972 52 00 69</a> | <a href="tel:0034972520069">+34 972 52 00 69</a> · e-mail <a href="mailto:&#105;&#110;&#102;&#111;&#064;&#104;&#111;&#116;&#101;&#108;&#101;&#108;&#109;&#111;&#108;&#105;&#046;&#099;&#111;&#109;"><!-- encoded - wbwip.com/wbw/emailencoder.html --><!-- info@hotelelmoli.com -->&#105;&#110;&#102;&#111;&#064;&#104;&#111;&#116;&#101;&#108;&#101;&#108;&#109;&#111;&#108;&#105;&#046;&#099;&#111;&#109;</a></p>
            </div>
            
            <a id="back-to-top" href="#"></a>

            <div class="sub-footer">
                <div class="container">
                    <span>© Hotel El Molí</span> - <span>Registre Turístic HG 001423 03</span> - <span><a class="legal" href="avis-legal.html" title="Avís Legal" rel="modal:open">Avís Legal</a></span> - <span><a href="http://sokvist.com" title="Sokvist, Web &amp; SEO">Sokvist, Web &amp; SEO</a></span>
                </div>
            </div>
		</footer>
        
	</main> <!-- /.main-content -->
    
    
    <script src="/assets/js/vendor/jquery-1.11.2.min.js"></script>
    <script type="text/javascript">
        //<![CDATA[
		$(window).load(function() { // makes sure the whole site is loaded
			$('#status').fadeOut(); // will first fade out the loading animation
			$('#preloader').delay(350).fadeOut(3000); // will fade out the white DIV that covers the website.
		})
		//]]>
    </script>
	<script src="/assets/js/plugins.js"></script>
    <script src="/assets/js/main.js"></script>
    
    <!-- analytics
    <script>
		(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
		ga('send', 'pageview');
    </script> -->
    
</body>
</html>