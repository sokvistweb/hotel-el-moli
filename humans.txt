# humanstxt.org/
# The humans responsible & technology colophon

# TEAM
Your title: Sokvist
Site: http://sokvist.com/
Twitter: @SokvistWeb
Location: Ventalló, Catalunya

# SITE
Last update: 15/06/2016 
Standards: HTML5, CSS3
Components: jQuery, LESS
Software: Brackets
   