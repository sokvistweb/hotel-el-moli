<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" lang="ca"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" lang="ca"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" lang="ca"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" lang="ca"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="ca"><!--<![endif]-->
<head>
    <meta charset="utf-8">
	<title>Hotel El Molí</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="Hotel familiar a la Costa Brava, situat a 2 minuts de la platja de Sant Pere Pescador i del parc Natural dels Aiguamolls de l'Empordà." />
    <meta name="keywords" content="Hotel, Costa Brava, Girona, Empordà, Sant Pere Pescador" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="author" href="humans.txt">
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="apple-touch-icon" href="apple-touch-icon.png" /><!-- 57×57px -->
    <link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png"><!-- 180×180px -->
    
    <link rel="stylesheet" href="assets/css/style.css">
	<link href='http://fonts.googleapis.com/css?family=Roboto:500,700,300' rel='stylesheet' type='text/css'>
    
    <link rel="canonical" href="http://hotelelmoli.com/">
    <link hreflang="es" href="http://hotelelmoli.com/es/" rel="alternate">
    <link hreflang="en" href="http://hotelelmoli.com/en/" rel="alternate">
    <link hreflang="fr" href="http://hotelelmoli.com/fr/" rel="alternate">
    <link hreflang="de" href="http://hotelelmoli.com/de/" rel="alternate">
    
    <!-- Open Graph -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="Hotel El Molí">
    <meta property="og:description" content="Hotel familiar a la Costa Brava, situat a 2 minuts de la platja de Sant Pere Pescador i del parc Natural dels Aiguamolls de l'Empordà.">
    <meta property="og:image" content="http://hotelelmoli.com/assets/images/web-og-image.jpg">
    <meta property="og:url" content="http://hotelelmoli.com/">
    <meta property="og:site_name" content="Hotel El Molí">
    <meta property="og:locale" content="ca_ES">
    
    <!-- Google+ / Schema.org 
    <link href="https://plus.google.com/+YourPage" rel="publisher">
    <meta itemprop="name" content="Hotel El Molí">
    <meta itemprop="description" content="Hotel familiar a la Costa Brava, situat a 2 minuts de la platja de Sant Pere Pescador i del parc Natural dels Aiguamolls de l'Empordà.">
    <meta itemprop="image" content="http://hotelelmoli.com/assets/images/og-image.jpg">
    -->

    <!-- Modernizr -->
	<script async src="assets/js/vendor/modernizr-2.7.1.min.js"></script>
  	
	
</head>
<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status" class="spinner">
            <div id="loading">
                <div id="loading-center">
                    <div id="loading-center-absolute">
                        <div class="object" id="object_one"></div>
                        <div class="object_black" id="black_one"></div>
                        <div class="object" id="object_two"></div>
                        <div class="object_black" id="black_two"></div>
                        <div class="object" id="object_three"></div>
                        <div class="object_black" id="black_three"></div>
                        <div class="object" id="object_four"></div>
                        <div class="object_black" id="black_four"></div>
                        <div class="object" id="object_five"></div>
                        <div class="object_black" id="black_five"></div>
                        <div class="object" id="object_six"></div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- /#preloader -->
    
	<header class="skv-header">
		<div id="logo"><a href="/"><img src="assets/images/svg/logo-hotel-el-moli.svg" alt="Logo Hotel El Molí"></a></div>
		<nav class="main-nav">
			<ul id="nav">
				<li><a href="#hotel">L'hotel</a></li>
				<li><a href="#habitacions">Habitacions</a></li>
				<li><a href="#serveis">Serveis</a></li>
				<li><a href="#preus">Preus</a></li>
			</ul>
		</nav> <!-- /.main-nav -->
        <nav class="languages">
            <ul>
                <li><a href="es/">es</a></li>
                <li><a href="en/">en</a></li>
                <li><a href="fr/">fr</a></li>
                <li><a href="de/">de</a></li>
            </ul>
        </nav>
        <div class="contact">
            <a href="tel:0034972520069">+34 972 52 00 69</a>
        </div>
	</header>

	<main class="main-content">
        <!-- Hotel starts -->
		<div class="fixed-bg bg-hotel" id="hotel">
			<h1>hotel el molí</h1>
            <div class="more-btn">
                <a href="#wellcome" class="discover"><span>Descobreix</span></a>
            </div>
		</div> <!-- /.fixed-bg -->

		<div class="scrolling-bg" id="wellcome">
            <div class="container">
                <p class="copy-home"><strong>Hotel familiar</strong> dins un espai de gairebé 20.000m2 de terreny. Situat a les afores del poble a pocs metres del riu Fluvià i de la zona del parc Natural dels <strong>Aiguamolls de l'Empordà</strong>. Tenim al costat l'extensa platja de dunes del nostre poble on practicar el windsurf, el <strong>kitesurf</strong>... Estem en una zona privilegiada on es poden practicar els més diversos esports combinant-ho amb una gran diversitat de sortides culturals.</p>

                <div class="branches branch1"></div>

                <div class="buttons">
                    <a class="booking" href="https://www.thebookingbutton.co.uk/properties/hotelelmolidirect" title="Pàgina de reserves" target="_blank">Reserves</a>
                    <a class="offers" href="oferta.php" title="Ofertes" rel="modal:open">Oferta de Primavera</a>
                </div>
            </div><!-- /.container -->
		</div> <!-- /.scrolling-bg -->
        <!-- Hotel ends -->
        
        
        <!-- Habitacions starts -->
        <div class="fixed-bg fixed-bg-90 bg-rooms" id="habitacions">
            <h2>habitacions</h2>
		</div> <!-- cd-fixed-bg -->

		<div class="scrolling-bg">
			<div class="container">
                <div class="trans-content">
                    <p>Disposem de vint-i-sis confortables habitacions, algunes d'elles tenen banyera d'hidromassatge, terrassa, d'altres estan a la planta baixa davant el jardí i la piscina, totes tenen TV satèl·lit i aire condicionat.</p>
                    <p>Podem també oferir-los habitacions familiars. Hem volgut donar un caire especial a cada habitació amb una acurada i personal decoració.</p>
                    
                    <nav class="rooms-nav">
                        <ul id="roomsnav">
                            <li><a class="booknow" href="#basica">Habitació doble bàsica</a></li>
                            <li><a class="booknow" href="#estandard">Habitació doble estandard</a></li>
                            <li><a class="booknow" href="#hidromassatge">Habitació doble superior</a></li>
                        </ul>
                    </nav>
                    
                    <div class="branches branch2"></div>
                </div>
            </div> <!-- cd-container -->

			<div class="container">
                <div class="slider" id="basica">
                    <h2>Habitació doble bàsica</h2>
                    <ul class="rslides" id="slider1">
                        <li><img src="assets/images/rooms/habitacio-basica-1.jpg" alt="Habitació doble bàsica" width="768" height="513"></li>
                        <li><img src="assets/images/rooms/habitacio-basica-2.jpg" alt="Habitació doble bàsica" width="768" height="513"></li>
                        <li><img src="assets/images/rooms/habitacio-basica-3.jpg" alt="Habitació doble bàsica" width="768" height="513"></li>
                        <li><img src="assets/images/rooms/habitacio-basica-4.jpg" alt="Habitació doble bàsica" width="768" height="513"></li>
                    </ul>
                    <!-- Slideshow 3 Pager -->
                    <ul class="slider-pager" id="slider1-pager">
                        <li><a href="#"><img src="assets/images/rooms/th-habitacio-basica-1.jpg" alt="Habitació doble bàsica" width="120" height="80"></a></li>
                        <li><a href="#"><img src="assets/images/rooms/th-habitacio-basica-2.jpg" alt="Habitació doble bàsica" width="120" height="80"></a></li>
                        <li><a href="#"><img src="assets/images/rooms/th-habitacio-basica-3.jpg" alt="Habitació doble bàsica" width="120" height="80"></a></li>
                        <li><a href="#"><img src="assets/images/rooms/th-habitacio-basica-4.jpg" alt="Habitació doble bàsica" width="120" height="80"></a></li>
                    </ul>
                    
                    <p>Habitació amb llit de matrimoni (135 cm) bany amb banyera, amenities, secador de cabell, TV pantalla plana amb Canals Satèl·lit, wifi gratuït, aire condicionat, 10m2 aprox.</p>

                    <a class="booknow" href="https://www.thebookingbutton.co.uk/properties/hotelelmolidirect" target="_blank" title="Pàgina de reserves">Reserva ara</a>
                </div> <!-- .slider -->
                
                <div class="slider" id="estandard">
                    <h2>Habitació doble estandard</h2>
                    <ul class="rslides" id="slider2">
                        <li><img src="assets/images/rooms/habitacio-estandard-1.jpg" alt="Habitació doble estandard" width="768" height="513"></li>
                        <li><img src="assets/images/rooms/habitacio-estandard-2.jpg" alt="Habitació doble estandard" width="768" height="513"></li>
                        <li><img src="assets/images/rooms/habitacio-estandard-3.jpg" alt="Habitació doble estandard" width="768" height="513"></li>
                        <li><img src="assets/images/rooms/habitacio-estandard-4.jpg" alt="Habitació doble estandard" width="768" height="513"></li>
                        <li><img src="assets/images/rooms/habitacio-estandard-5.jpg" alt="Habitació doble estandard" width="768" height="513"></li>
                        <li><img src="assets/images/rooms/habitacio-estandard-6.jpg" alt="Habitació doble estandard" width="768" height="513"></li>
                    </ul>
                    <!-- Slideshow 3 Pager -->
                    <ul class="slider-pager" id="slider2-pager">
                        <li><a href="#"><img src="assets/images/rooms/th-habitacio-estandard-1.jpg" alt="Habitació doble estandard" width="120" height="80"></a></li>
                        <li><a href="#"><img src="assets/images/rooms/th-habitacio-estandard-2.jpg" alt="Habitació doble estandard" width="120" height="80"></a></li>
                        <li><a href="#"><img src="assets/images/rooms/th-habitacio-estandard-3.jpg" alt="Habitació doble estandard" width="120" height="80"></a></li>
                        <li><a href="#"><img src="assets/images/rooms/th-habitacio-estandard-4.jpg" alt="Habitació doble estandard" width="120" height="80"></a></li>
                        <li><a href="#"><img src="assets/images/rooms/th-habitacio-estandard-5.jpg" alt="Habitació doble estandard" width="120" height="80"></a></li>
                        <li><a href="#"><img src="assets/images/rooms/th-habitacio-estandard-6.jpg" alt="Habitació doble estandard" width="120" height="80"></a></li>
                    </ul>
                    
                    <p>Habitació amb llit de matrimoni (150 cm) o dos llits individuals, bany amb banyera o dutxa, amb balcó o sense, amenities, TV pantalla plana amb Canals Satèl·lit, wifi gratuït, aire condicionat, 14 m2 aprox.</p>
                    
                    <a class="booknow" href="https://www.thebookingbutton.co.uk/properties/hotelelmolidirect" target="_blank" title="Pàgina de reserves">Reserva ara</a>
                </div> <!-- /.slider -->
                
                <div class="slider" id="hidromassatge">
                    <h2>Habitació doble superior</h2>
                    <ul class="rslides" id="slider3">
                        <li><img src="assets/images/rooms/habitacio-superior-1.jpg" alt="Habitació doble superior" width="768" height="513"></li>
                        <li><img src="assets/images/rooms/habitacio-superior-2.jpg" alt="Habitació doble superior" width="768" height="513"></li>
                        <li><img src="assets/images/rooms/habitacio-superior-3.jpg" alt="Habitació doble superior" width="768" height="513"></li>
                        <li><img src="assets/images/rooms/habitacio-superior-4.jpg" alt="Habitació doble superior" width="768" height="513"></li>
                        <li><img src="assets/images/rooms/habitacio-superior-5.jpg" alt="Habitació doble superior" width="768" height="513"></li>
                        <li><img src="assets/images/rooms/habitacio-superior-6.jpg" alt="Habitació doble superior" width="768" height="513"></li>
                    </ul>
                    <!-- Slideshow 3 Pager -->
                    <ul class="slider-pager" id="slider3-pager">
                        <li><a href="#"><img src="assets/images/rooms/th-habitacio-superior-1.jpg" alt="Habitació doble superior" width="120" height="80"></a></li>
                        <li><a href="#"><img src="assets/images/rooms/th-habitacio-superior-2.jpg" alt="Habitació doble superior" width="120" height="80"></a></li>
                        <li><a href="#"><img src="assets/images/rooms/th-habitacio-superior-3.jpg" alt="Habitació doble superior" width="120" height="80"></a></li>
                        <li><a href="#"><img src="assets/images/rooms/th-habitacio-superior-4.jpg" alt="Habitació doble superior" width="120" height="80"></a></li>
                        <li><a href="#"><img src="assets/images/rooms/th-habitacio-superior-5.jpg" alt="Habitació doble superior" width="120" height="80"></a></li>
                        <li><a href="#"><img src="assets/images/rooms/th-habitacio-superior-6.jpg" alt="Habitació doble superior" width="120" height="80"></a></li>
                    </ul>
                    
                    <p>Habitació amb llit de matrimoni (150 cm) o dos llits individuals, bany amb banyera  d´hidromassatge, aire condicionat, amb balcó  o planta baixa , amenities, TV pantalla plana, Canals Satèl·lit, wifi gratuït,20 m2 aprox.</p>

                    <a class="booknow" href="https://www.thebookingbutton.co.uk/properties/hotelelmolidirect" target="_blank" title="Pàgina de reserves">Reserva ara</a>
                </div> <!-- /.slider -->
            </div> <!-- /.container -->
		</div> <!-- /.scrolling-bg -->
        <!-- Habitacions ends -->
        
        
        <!-- Serveis starts -->
        <div class="fixed-bg fixed-bg-90 bg-services" id="serveis">
			<h2>serveis</h2>
		</div> <!-- /.fixed-bg -->

		<div class="scrolling-bg">
            <div class="container">
                <p>Els nostres clients poden disposar de pàrquing propi, piscina, parc infantil, àmplia zona de solàrium i terrassa, servei de Snack-Bar tot el dia, zona wifi, biblioteca. Poden practicar el tenis i el frontó a les nostres instal·lacions, disposem també d’un garatge per guardar estris nàutics, bicicletes...</p>
                <p>L’hotel està rodejat d’una àmplia zona de jardí i d’un bosc de pins on passejar o deixar-se seduir per una bona lectura.</p>

                <div class="branches branch3"></div>
            </div><!-- /.container -->
            
            <div class="container container-1024">
                <div class="gallery">
                    <a href="assets/images/gallery/hotel-el-moli-02.jpg" class="swipebox thumbnail"><img class="th-image" src="assets/images/gallery/th_hotel-el-moli-02.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="assets/images/gallery/hotel-el-moli-03.jpg" class="swipebox thumbnail"><img class="th-image" src="assets/images/gallery/th_hotel-el-moli-03.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="assets/images/gallery/hotel-el-moli-04.jpg" class="swipebox thumbnail"><img class="th-image" src="assets/images/gallery/th_hotel-el-moli-04.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="assets/images/gallery/hotel-el-moli-05.jpg" class="swipebox thumbnail"><img class="th-image" src="assets/images/gallery/th_hotel-el-moli-05.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="assets/images/gallery/hotel-el-moli-09.jpg" class="swipebox thumbnail"><img class="th-image" src="assets/images/gallery/th_hotel-el-moli-09.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="assets/images/gallery/hotel-el-moli-10.jpg" class="swipebox thumbnail"><img class="th-image" src="assets/images/gallery/th_hotel-el-moli-10.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="assets/images/gallery/hotel-el-moli-11.jpg" class="swipebox thumbnail"><img class="th-image" src="assets/images/gallery/th_hotel-el-moli-11.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="assets/images/gallery/hotel-el-moli-12.jpg" class="swipebox thumbnail"><img class="th-image" src="assets/images/gallery/th_hotel-el-moli-12.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="assets/images/gallery/hotel-el-moli-13.jpg" class="swipebox thumbnail"><img class="th-image" src="assets/images/gallery/th_hotel-el-moli-13.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="assets/images/gallery/hotel-el-moli-14.jpg" class="swipebox thumbnail"><img class="th-image" src="assets/images/gallery/th_hotel-el-moli-14.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="assets/images/gallery/hotel-el-moli-15.jpg" class="swipebox thumbnail"><img class="th-image" src="assets/images/gallery/th_hotel-el-moli-15.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="assets/images/gallery/hotel-el-moli-16.jpg" class="swipebox thumbnail"><img class="th-image" src="assets/images/gallery/th_hotel-el-moli-16.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="assets/images/gallery/hotel-el-moli-17.jpg" class="swipebox thumbnail"><img class="th-image" src="assets/images/gallery/th_hotel-el-moli-17.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="assets/images/gallery/hotel-el-moli-18.jpg" class="swipebox thumbnail"><img class="th-image" src="assets/images/gallery/th_hotel-el-moli-18.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="assets/images/gallery/hotel-el-moli-19.jpg" class="swipebox thumbnail"><img class="th-image" src="assets/images/gallery/th_hotel-el-moli-19.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="assets/images/gallery/hotel-el-moli-20.jpg" class="swipebox thumbnail"><img class="th-image" src="assets/images/gallery/th_hotel-el-moli-20.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="assets/images/gallery/hotel-el-moli-21.jpg" class="swipebox thumbnail"><img class="th-image" src="assets/images/gallery/th_hotel-el-moli-21.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="assets/images/gallery/hotel-el-moli-22.jpg" class="swipebox thumbnail"><img class="th-image" src="assets/images/gallery/th_hotel-el-moli-22.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="assets/images/gallery/hotel-el-moli-23.jpg" class="swipebox thumbnail"><img class="th-image" src="assets/images/gallery/th_hotel-el-moli-23.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                    <a href="assets/images/gallery/hotel-el-moli-24.jpg" class="swipebox thumbnail"><img class="th-image" src="assets/images/gallery/th_hotel-el-moli-24.jpg" width="200" height="133" alt="Hotel El Molí"></a>
                </div>
            </div><!-- /.container -->
		</div> <!-- /.scrolling-bg -->
        <!-- Servies ends -->

        
        
        <!-- Preus starts -->
        <div class="fixed-bg fixed-bg-90 bg-prices" id="preus">
			<h2>preus</h2>
		</div> <!-- /.fixed-bg -->

		<div class="scrolling-bg">
            <div class="container container-1024">
                
                <!-- Prices -->
                <?php include 'preus.php';?>
                
                <div class="table-columns">
                    <ul class="price">
                        <li class="header">
                            <h2>Temporada baixa</h2>
                            <p>De l’1 de Juny al 30 de Juny i <br>del 28 d'Agost al l'1 d'Octubre</p>
                        </li>
                        <li>
                            <p>Habitació doble bàsica</p>
                            <span><?=$tb2b?> €</span>
                        </li>
                        <li class="grey">
                            <p>Habitació doble estandard</p>
                            <span><?=$tb2e?> €</span>
                        </li>
                        <li>
                            <p>Habitació doble superior</p>
                            <span><?=$tb2s?> €</span>
                        </li>
                        <li class="grey">
                            <p>Habitació triple</p>
                            <span><?=$tb3e?> €</span>
                        </li>
                        <li>
                            <p>Habitació triple superior</p>
                            <span><?=$tb3s?> €</span>
                        </li>
                        <li class="grey">
                            <p>Esmorzar</p>
                            <span><?=$tbes?> €</span>
                        </li>
                        <li class="grey">
                            <p>Llit supletori</p>
                            <span><?=$tbls?> €</span>
                        </li>
                        <li><a href="https://www.thebookingbutton.co.uk/properties/hotelelmolidirect" target="_blank" title="Pàgina de reserves" class="button">Reserva ara</a></li>
                    </ul>
                </div>
                
                
                <div class="table-columns">
                    <ul class="price">
                        <li class="header">
                            <h2>Temporada mitja</h2>
                            <p>Del 6 d’Abril al 10 d’Abril i <br>de l'1 de Juliol al 31 de Juliol</p>
                        </li>
                        <li>
                            <p>Habitació doble bàsica</p>
                            <span><?=$tm2b?> €</span>
                        </li>
                        <li class="grey">
                            <p>Habitació doble estandard</p>
                            <span><?=$tm2e?> €</span>
                        </li>
                        <li>
                            <p>Habitació doble superior</p>
                            <span><?=$tm2s?> €</span>
                        </li>
                        <li class="grey">
                            <p>Habitació triple</p>
                            <span><?=$tm3e?> €</span>
                        </li>
                        <li>
                            <p>Habitació triple superior</p>
                            <span><?=$tm3s?> €</span>
                        </li>
                        <li class="grey">
                            <p>Esmorzar</p>
                            <span><?=$tmes?> €</span>
                        </li>
                        <li class="grey">
                            <p>Llit supletori</p>
                            <span><?=$tmls?> €</span>
                        </li>
                        <li><a href="https://www.thebookingbutton.co.uk/properties/hotelelmolidirect" target="_blank" title="Pàgina de reserves" class="button">Reserva ara</a></li>
                    </ul>
                </div>


                <div class="table-columns">
                    <ul class="price">
                        <li class="header">
                            <h2>Temporada alta</h2>
                            <p>De l’1 d’Agost <br>al 27 d’Agost</p>
                        </li>
                        <li>
                            <p>Habitació doble bàsica</p>
                            <span><?=$ta2b?> €</span>
                        </li>
                        <li class="grey">
                            <p>Habitació doble estandard</p>
                            <span><?=$ta2e?> €</span>
                        </li>
                        <li>
                            <p>Habitació doble superior</p>
                            <span><?=$ta2s?> €</span>
                        </li>
                        <li class="grey">
                            <p>Habitació triple</p>
                            <span><?=$ta3e?> €</span>
                        </li>
                        <li>
                            <p>Habitació triple superior</p>
                            <span><?=$ta3s?> €</span>
                        </li>
                        <li class="grey">
                            <p>Esmorzar</p>
                            <span><?=$taes?> €</span>
                        </li>
                        <li class="grey">
                            <p>Llit supletori</p>
                            <span><?=$tals?> €</span>
                        </li>
                        <li><a href="https://www.thebookingbutton.co.uk/properties/hotelelmolidirect" target="_blank" title="Pàgina de reserves" class="button">Reserva ara</a></li>
                    </ul>
                </div>
                

                <div class="align-right">
                    <p>10% IVA inclòs / + Taxa turística 0.66 € per persona</p>
                </div>
        
                <div class="buttons">
                    <a class="offers" href="oferta.php" title="Ofertes" rel="modal:open">Oferta de Primavera</a>
                </div>
        
                <div class="branches branch2 branch3em"></div>
        
                <div class="align-center">
                    <p><strong>Reserves: tel. <a href="tel:0034972520069">+34 972 52 00 69</a> · e-mail <a href="tel:0034972520069">+34 972 52 00 69</a> · e-mail <a href="mailto:&#105;&#110;&#102;&#111;&#064;&#104;&#111;&#116;&#101;&#108;&#101;&#108;&#109;&#111;&#108;&#105;&#046;&#099;&#111;&#109;"><!-- encoded - wbwip.com/wbw/emailencoder.html --><!-- info@hotelelmoli.com -->&#105;&#110;&#102;&#111;&#064;&#104;&#111;&#116;&#101;&#108;&#101;&#108;&#109;&#111;&#108;&#105;&#046;&#099;&#111;&#109;</a></strong></p>
                </div>
            </div><!-- /.container -->
		</div> <!-- /.scrolling-bg -->
        <!-- Preus ends -->
        

		<div class="fixed-bg fixed-bg-75 bg-hammock">
		</div> <!-- /.fixed-bg -->
    

		<footer>
            <div class="cont-footer">
                <p>Hotel El Molí | Ctra. de la Platja 3 | 17470 Sant Pere Pescador | Girona, Alt Empordà, Costa Brava <a href="https://goo.gl/maps/fKerikuJARQ2" title="Veure a Google Maps" target="_blank"><img src="assets/images/svg/ios-location.svg" alt="On som" width="512" height="512"></a></p>
                <p>Tel./Fax: <a href="tel:0034972520069">+34 972 52 00 69</a> | <a href="tel:0034972520069">+34 972 52 00 69</a> · e-mail <a href="mailto:&#105;&#110;&#102;&#111;&#064;&#104;&#111;&#116;&#101;&#108;&#101;&#108;&#109;&#111;&#108;&#105;&#046;&#099;&#111;&#109;"><!-- encoded - wbwip.com/wbw/emailencoder.html --><!-- info@hotelelmoli.com -->&#105;&#110;&#102;&#111;&#064;&#104;&#111;&#116;&#101;&#108;&#101;&#108;&#109;&#111;&#108;&#105;&#046;&#099;&#111;&#109;</a></p>
            </div>
            
            <a id="back-to-top" href="#"></a>

            <div class="sub-footer">
                <div class="container">
                    <span>© Hotel El Molí</span> - <span>Registre Turístic HG 001423 03</span> - <span><a class="legal" href="avis-legal.html" title="Avís Legal" rel="modal:open">Avís Legal</a></span> - <span><a href="http://sokvist.com" title="Sokvist, Web &amp; SEO">Sokvist, Web &amp; SEO</a></span>
                </div>
            </div>
		</footer>
        
	</main> <!-- /.main-content -->
    
    
	<script src="assets/js/vendor/jquery-1.11.2.min.js"></script>
    <script type="text/javascript">
        //<![CDATA[
		$(window).load(function() { // makes sure the whole site is loaded
			$('#status').fadeOut(); // will first fade out the loading animation
			$('#preloader').delay(350).fadeOut(3000); // will fade out the white DIV that covers the website.
		})
		//]]>
    </script>
	<script src="assets/js/plugins.js"></script>
    <script src="assets/js/main.js"></script>
    
    <!-- analytics
    <script>
		(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
		ga('send', 'pageview');
    </script> -->
    
</body>
</html>