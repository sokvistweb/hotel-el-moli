

    <div class="modal-wrapper">
        
        <!-- Prices -->
        <?php include 'preus.php';?>
        
        <div class="modal-container">
            <div class="row lined">
                <div class="column">
                    <h2>Oferta de primavera</h2>
                    <p><strong>Del 23 d'Abril fins el 31 de Maig</strong></p>
                </div>
            </div>
            
            <div class="row">
                <div class="column column-75">
                    <p>Habitació doble bàsica</p>
                </div>
                <div class="column column-25">
                    <p><strong><?=$so2b?> €</strong></p>
                </div>
            </div>
            <div class="row lined">
                <div class="column column-75">
                    <p>Habitació doble estandard</p>
                </div>
                <div class="column column-25">
                    <p><strong><?=$so2e?> €</strong></p>
                </div>
            </div>
            <div class="row">
                <div class="column column-75">
                    <p>Habitació doble superior</p>
                </div>
                <div class="column column-25">
                    <p><strong><?=$so2s?> €</strong></p>
                </div>
            </div>
            <div class="row lined">
                <div class="column column-75">
                    <p>Habitació triple estandard</p>
                </div>
                <div class="column column-25">
                    <p><strong><?=$so3e?> €</strong></p>
                </div>
            </div>
            <div class="row">
                <div class="column column-75">
                    <p>Habitació triple superior</p>
                </div>
                <div class="column column-25">
                    <p><strong><?=$so3s?> €</strong></p>
                </div>
            </div>
        </div><!-- /.modal-container -->
        
        <div class="align-center">
            <div class="buttons-popup">
                <a class="booking-popup" href="https://www.thebookingbutton.co.uk/properties/hotelelmolidirect" title="Pàgina de reserves" target="_blank">Reserves</a>
            </div>
            <!--<p><strong>Reserves: tel. <a href="tel:0034972520069">+34 972 52 00 69</a> · e-mail <a href="mailto:&#105;&#110;&#102;&#111;&#064;&#104;&#111;&#116;&#101;&#108;&#101;&#108;&#109;&#111;&#108;&#105;&#046;&#099;&#111;&#109;"><!-- encoded - wbwip.com/wbw/emailencoder.html --><!-- info@hotelelmoli.com --><!--&#105;&#110;&#102;&#111;&#064;&#104;&#111;&#116;&#101;&#108;&#101;&#108;&#109;&#111;&#108;&#105;&#046;&#099;&#111;&#109;</a></strong></p>-->
        </div>
        
    </div> <!-- sk-modal -->

    